package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CounterTest {

    @Test
    public void testDivide() {
        assertEquals("divide zero", Integer.MAX_VALUE, new Counter().divide(4, 0));
    }

    @Test
    public void testDivide_zero() {
        assertEquals("divide zero", 3, new Counter().divide(6, 2));
    }
}

